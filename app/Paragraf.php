<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paragraf extends Model
{
    protected $guarded = ['id'];
    protected $table = 'paragraf';
}
