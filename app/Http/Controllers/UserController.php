<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    public function index()
    {
        $users = Cache::remember('users', 120,  function(){
            return User::all();
        });

        return view('user', compact('users'));
    }

    public function tanpaCache()
    {
        $users = User::all();
        return view('user', compact('users'));
    }
}
