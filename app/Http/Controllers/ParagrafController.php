<?php

namespace App\Http\Controllers;

use App\Paragraf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ParagrafController extends Controller
{
    public function index()
    {
        return view('paragraf');
    }

    public function store(Request $request)
    {
        $kata = explode(" ", $request->paragraf);
        foreach($kata as $data)
        {
            Paragraf::create(['testimoni'=>$data]);
        }

        return redirect()->back();
    }

    public function getData()
    {
        $kata = Paragraf::all();
        return view('index', compact('kata'));
    }

    public function cacheRedis()
    {
        $kata = Cache::remember('kata', 60, function(){
            return Paragraf::all();
        });
        return view('index', compact('kata'));
    }
}
