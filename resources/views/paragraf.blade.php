<!doctype html>

<html>
    <head>
        <title>Belajar Cache</title>
    </head>
    <body>
        <h3>Masukkan Paragraf</h3>

        <form action="{{url('paragraf')}}" method="post">
        @csrf
            <div>
                <label>Judul </label>
                <input type="text" name="judul" placeholder="ketik judul">
            </div>
            <div>
                <label>Paragraf </label>
                <textarea name="paragraf" placeholder="ketik paragraf"></textarea>
            </div>
            <button type="submit">Simpan</button>
        </form>
    </body>
</html>