<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {

    // $users = Cache::get('users', function(){
    //     return User::all();
    // });
    // dd($users);

    return view('welcome');
});

Route::get('user', 'UserController@index');
Route::get('paragraf', 'ParagrafController@index');
Route::get('index', 'ParagrafController@getData');
Route::get('index-cache', 'ParagrafController@cacheRedis');
Route::post('paragraf', 'ParagrafController@store');
Route::get('tanpa-cache', 'UserController@tanpaCache');
